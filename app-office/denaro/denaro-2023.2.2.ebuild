# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_PN=NickvisionMoney
MY_P=${MY_PN}-${PV}

DOTNET_COMPAT=7.0
PYTHON_COMPAT=( python3_{9..11} )

NUGETS="
docnet.core-2.3.1
gircore.adw-1-0.3.0
gircore.cairo-1.0-0.3.0
gircore.freetype2-2.0-0.3.0
gircore.gdk-4.0-0.3.0
gircore.gdkpixbuf-2.0-0.3.0
gircore.gio-2.0-0.3.0
gircore.glib-2.0-0.3.0
gircore.gobject-2.0-0.3.0
gircore.graphene-1.0-0.3.0
gircore.gsk-4.0-0.3.0
gircore.gtk-4.0-0.3.0
gircore.harfbuzz-0.0-0.3.0
gircore.pango-1.0-0.3.0
harfbuzzsharp-2.8.2.3
harfbuzzsharp.nativeassets.linux-2.8.2.3
harfbuzzsharp.nativeassets.macos-2.8.2.3
harfbuzzsharp.nativeassets.win32-2.8.2.3
hazzik.qif-1.0.3
microsoft.aspnetcore.app.runtime.linux-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-x64-7.0.3
microsoft.aspnetcore.app.runtime.linux-x64-7.0.3
microsoft.data.sqlite.core-7.0.3
microsoft.netcore.app.host.linux-arm64-7.0.3
microsoft.netcore.app.host.linux-arm-7.0.3
microsoft.netcore.app.host.linux-musl-arm64-7.0.3
microsoft.netcore.app.host.linux-musl-arm-7.0.3
microsoft.netcore.app.host.linux-musl-x64-7.0.3
microsoft.netcore.app.runtime.linux-arm64-7.0.3
microsoft.netcore.app.runtime.linux-arm-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm-7.0.3
microsoft.netcore.app.runtime.linux-musl-x64-7.0.3
microsoft.netcore.app.runtime.linux-x64-7.0.3
microsoft.netcore.platforms-1.1.0
microsoft.netcore.platforms-5.0.0
microsoft.netcore.targets-5.0.0
microsoft.win32.primitives-4.3.0
netstandard.library-1.6.1
ofxsharp.netstandard-1.0.0
questpdf-2022.12.1
readsharp.ports.sgmlreader.core-1.0.0
runtime.debian.8-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.23-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.24-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system-4.3.0
runtime.native.system.io.compression-4.3.0
runtime.native.system.net.http-4.3.0
runtime.native.system.security.cryptography.apple-4.3.0
runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.13.2-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.42.1-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.apple-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.rhel.7-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.14.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
sixlabors.imagesharp-2.1.3
skiasharp-2.88.3
skiasharp.harfbuzz-2.88.3
skiasharp.nativeassets.linux-2.88.3
skiasharp.nativeassets.macos-2.88.3
skiasharp.nativeassets.win32-2.88.3
sqlitepclraw.bundle_e_sqlcipher-2.1.4
sqlitepclraw.core-2.1.4
sqlitepclraw.lib.e_sqlcipher-2.1.4
sqlitepclraw.provider.e_sqlcipher-2.1.4
system.appcontext-4.3.0
system.buffers-4.3.0
system.collections-4.3.0
system.collections.concurrent-4.3.0
system.console-4.3.0
system.diagnostics.debug-4.3.0
system.diagnostics.diagnosticsource-4.3.0
system.diagnostics.tools-4.3.0
system.diagnostics.tracing-4.3.0
system.globalization-4.3.0
system.globalization.calendars-4.3.0
system.globalization.extensions-4.3.0
system.io-4.3.0
system.io.compression-4.3.0
system.io.compression.zipfile-4.3.0
system.io.filesystem-4.3.0
system.io.filesystem.primitives-4.3.0
system.linq-4.3.0
system.linq.expressions-4.3.0
system.memory-4.5.3
system.net.http-4.3.0
system.net.primitives-4.3.0
system.net.requests-4.3.0
system.net.sockets-4.3.0
system.net.webheadercollection-4.3.0
system.objectmodel-4.3.0
system.reflection-4.3.0
system.reflection.emit-4.3.0
system.reflection.emit.ilgeneration-4.3.0
system.reflection.emit.lightweight-4.3.0
system.reflection.extensions-4.3.0
system.reflection.primitives-4.3.0
system.reflection.typeextensions-4.3.0
system.resources.resourcemanager-4.3.0
system.runtime-4.3.0
system.runtime.compilerservices.unsafe-5.0.0
system.runtime.extensions-4.3.0
system.runtime.handles-4.3.0
system.runtime.interopservices-4.3.0
system.runtime.interopservices.runtimeinformation-4.3.0
system.runtime.numerics-4.3.0
system.security.cryptography.algorithms-4.3.0
system.security.cryptography.cng-4.3.0
system.security.cryptography.csp-4.3.0
system.security.cryptography.encoding-4.3.0
system.security.cryptography.openssl-4.3.0
system.security.cryptography.primitives-4.3.0
system.security.cryptography.x509certificates-4.3.0
system.text.encoding-4.3.0
system.text.encoding.codepages-5.0.0
system.text.encoding.extensions-4.3.0
system.text.regularexpressions-4.3.0
system.threading-4.3.0
system.threading.tasks-4.3.0
system.threading.tasks.extensions-4.3.0
system.threading.timer-4.3.0
system.xml.readerwriter-4.3.0
system.xml.xdocument-4.3.0
"

inherit dotnet-pkg gnome2-utils python-any-r1 xdg

DESCRIPTION="A personal finance manager"
HOMEPAGE="https://github.com/nlogozzo/NickvisionMoney/"
SRC_URI="
	https://github.com/nlogozzo/${MY_PN}/archive/${PV}.tar.gz
		-> ${P}.tar.gz
	$(nuget_uris)
"
S="${WORKDIR}"/${MY_P}/${MY_PN}.GNOME

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	app-arch/brotli
	dev-db/sqlite:3
	dev-libs/glib
	gui-libs/gtk:4
	gui-libs/libadwaita:1
	media-libs/freetype
	media-libs/harfbuzz
"
BDEPEND="${PYTHON_DEPS}"

DOTNET_PROJECT="${S}/NickvisionMoney.GNOME.csproj"
PATCHES=(
	"${FILESDIR}"/${PN}-2023.2.2-desktop.patch
	"${FILESDIR}"/${PN}-2023.2.2-install_extras.patch
)

pkg_setup() {
	dotnet-pkg_pkg_setup
	python-any-r1_pkg_setup
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/${MY_PN}.GNOME
	dosym -r /usr/bin/${MY_PN}.GNOME /usr/bin/${PN}

	sh ./install_extras.sh "${ED}"/usr/ || die "script install_extras.sh failed"
	rm -r "${ED}"/usr/opt || die

	dodoc ../CONTRIBUTING.md ../README.md
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_pkg_postinst
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_pkg_postrm
}
