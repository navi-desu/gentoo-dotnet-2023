# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=6.0
NUGETS="
microsoft.aspnetcore.app.runtime.linux-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-x64-6.0.12
microsoft.aspnetcore.app.runtime.linux-x64-6.0.12
microsoft.netcore.app.host.linux-arm-6.0.12
microsoft.netcore.app.host.linux-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-arm-6.0.12
microsoft.netcore.app.host.linux-musl-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-arm-6.0.12
microsoft.netcore.app.runtime.linux-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-x64-6.0.12
"

inherit dotnet-pkg

DESCRIPTION="The compiler generator Coco/R for C#"
HOMEPAGE="https://github.com/boogie-org/coco/"
SRC_URI="
	https://github.com/boogie-org/${PN}/archive/${PV}.tar.gz
		-> ${P}.tar.gz
	$(nuget_uris)
"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64"

PATCHES=( "${FILESDIR}"/${P}-Coco-csproj.patch )

DOTNET_PROJECT="${S}/Coco.csproj"

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/${PN^} ${PN}

	einstalldocs
}
