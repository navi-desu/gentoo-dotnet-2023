# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=7.0
NUGETS="
microsoft.aspnetcore.app.ref-3.1.10
microsoft.aspnetcore.app.runtime.linux-arm-3.1.32
microsoft.aspnetcore.app.runtime.linux-arm64-3.1.32
microsoft.aspnetcore.app.runtime.linux-musl-arm64-3.1.32
microsoft.aspnetcore.app.runtime.linux-musl-x64-3.1.32
microsoft.aspnetcore.app.runtime.linux-x64-3.1.32
microsoft.netcore.app.host.linux-arm-3.1.32
microsoft.netcore.app.host.linux-arm64-3.1.32
microsoft.netcore.app.host.linux-musl-arm64-3.1.32
microsoft.netcore.app.host.linux-musl-x64-3.1.32
microsoft.netcore.app.host.linux-x64-3.1.32
microsoft.netcore.app.ref-3.1.0
microsoft.netcore.app.runtime.linux-arm-3.1.32
microsoft.netcore.app.runtime.linux-arm64-3.1.32
microsoft.netcore.app.runtime.linux-musl-arm64-3.1.32
microsoft.netcore.app.runtime.linux-musl-x64-3.1.32
microsoft.netcore.app.runtime.linux-x64-3.1.32
"

inherit dotnet-pkg wrapper

DESCRIPTION=".NET information tool for Gentoo"
HOMEPAGE="https://gitlab.com/xgqt/csharp-gentoodotnetinfo/"
SRC_URI="
	https://gitlab.com/xgqt/${PN}/-/archive/${PV}/${P}.tar.bz2
	$(nuget_uris)
"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64"

DOTNET_PROJECT="${S}/Source/gentoodotnetinfo/GentooDotnetInfo/GentooDotnetInfo.csproj"

src_install() {
	dotnet-pkg-utils_install

	make_wrapper gentoo-dotnet-info \
		"dotnet exec ${EPREFIX}/usr/share/${P}/GentooDotnetInfo.dll --"

	einstalldocs
}
