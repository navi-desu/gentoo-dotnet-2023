# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: dotnet-pkg.eclass
# @MAINTAINER:
# Gentoo Dotnet project <dotnet@gentoo.org>
# @AUTHOR:
# Anna Figueiredo Gomes <navi@vlhl.dev>
# Maciej Barć <xgqt@gentoo.org>
# @SUPPORTED_EAPIS: 7 8
# @PROVIDES: dotnet-pkg-utils
# @BLURB: common functions and variables for .NET packages
# @DESCRIPTION:
# This eclass is designed to help with building and installing packages that
# use the .NET SDK.
#
# .NET SDK is a open-source framework from Microsoft, it is a cross-platform
# successor to .NET Framework.
#
# .NET packages require proper inspection before packaging:
# - the compatible .NET SDK version has to be declared,
#   this can be done by inspecting the package's "*proj" files,
#   unlike JAVA, .NET packages tend to lock onto one selected .NET SDK
#   version, so building with other .NET versions will be mostly unsupported,
# - nugets, which are similar to JAVA's JARs (package .NET dependencies),
#   have to be listed using either the "NUGETS" variable or bundled inside
#   a "prebuilt" archive, in second case also the "NUGET_PACKAGES" variable
#   has to be explicitly set.
# - the main project file (.*proj) that builds the project has to be specified
#   by the "DOTNET_PROJECT" variable.

case "${EAPI}" in
	7 | 8 )
		:
		;;
	* )
		die "${ECLASS}: EAPI ${EAPI} unsupported."
		;;
esac

if [[ -z ${_DOTNET_PKG_ECLASS} ]] ; then
_DOTNET_PKG_ECLASS=1

inherit dotnet-pkg-utils

# @FUNCTION: dotnet-pkg_pkg_setup
# @DESCRIPTION:
# Default "pkg_setup" for the "dotnet-pkg" eclass.
# Pre-build configuration and checks.
#
# Calls "dotnet-pkg-utils_pkg_setup".
dotnet-pkg_pkg_setup() {
	dotnet-pkg-utils_setup
}

# @FUNCTION: dotnet-pkg_src_unpack
# @DESCRIPTION:
# Default "src_unpack" for the "dotnet-pkg" eclass.
# Unpack the package sources.
#
# Includes a special exception for nugets (".nupkg" files) - they are instead
# copied into the "NUGET_PACKAGES" directory.
dotnet-pkg_src_unpack() {
	local archive
	for archive in ${A} ; do
		case ${archive} in
			*.nupkg )
				cp "${DISTDIR}"/${archive} "${NUGET_PACKAGES}" || die
				;;
			* )
				unpack ${archive}
				;;
		esac
	done
}

# @FUNCTION: dotnet-pkg_src_prepare
# @DESCRIPTION:
# Default "src_prepare" for the "dotnet-pkg" eclass.
# Prepare the package sources.
#
# Run "dotnet-pkg-utils_remove-global-json".
dotnet-pkg_src_prepare() {
	dotnet-pkg-utils_remove-global-json

	default
}

# @FUNCTION: dotnet-pkg_src_configure
# @DESCRIPTION:
# Default "src_configure" for the "dotnet-pkg" eclass.
# Configure the package.
#
# First show information about current .NET SDK that is being used,
# then restore the project file specified by "DOTNET_PROJECT",
# afterwards restore any found solutions.
dotnet-pkg_src_configure() {
	dotnet-pkg-utils_info

	dotnet-pkg-utils_restore "$(dotnet-pkg-utils_get-project)"

	dotnet-pkg-utils_foreach-solution dotnet-pkg-utils_restore "$(pwd)"
}

# @FUNCTION: dotnet-pkg_src_compile
# @DESCRIPTION:
# Default "src_compile" for the "dotnet-pkg" eclass.
# Build the package.
#
# Build the package using "dotnet build" in the directory specified by either
# "DOTNET_PROJECT" or "S" variables.
#
# For more info see: "DOTNET_PROJECT" variable
# and "dotnet-pkg-utils_get-project" function.
dotnet-pkg_src_compile() {
	dotnet-pkg-utils_build "$(dotnet-pkg-utils_get-project)"
}

# @FUNCTION: dotnet-pkg_src_test
# @DESCRIPTION:
# Default "src_test" for the "dotnet-pkg" eclass.
# Test the package.
#
# Test the package by testing any found solutions.
dotnet-pkg_src_test() {
	dotnet-pkg-utils_foreach-solution dotnet-pkg-utils_test "$(pwd)"
}

# @FUNCTION: dotnet-pkg_src_install
# @DESCRIPTION:
# Default "src_install" for the "dotnet-pkg" eclass.
# Install the package.
#
# This is the default package install function for the "dotnet-pkg" eclass.
# It is very likely that this function is either insufficient or has to be
# redefined in a ebuild.
dotnet-pkg_src_install() {
	# Install the compiled .NET package artifacts,
	# for more info see "dotnet-pkg-utils_install" and "DOTNET_OUTPUT".
	dotnet-pkg-utils_install

	# Create launcher from the .NET package directory to "/usr/bin".
	# For example: /usr/bin/Nake -> /usr/share/nake-3.0.0/Nake
	dotnet-pkg-utils_dolauncher /usr/share/${P}/${PN^}

	# Create a compatibility symlink and also for ease of use from CLI.
	dosym -r /usr/bin/${PN^} /usr/bin/${PN}

	einstalldocs
}

EXPORT_FUNCTIONS pkg_setup src_unpack src_prepare src_configure src_compile src_test src_install

fi
