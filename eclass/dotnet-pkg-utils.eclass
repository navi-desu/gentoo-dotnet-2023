# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: dotnet-pkg-utils.eclass
# @MAINTAINER:
# Gentoo Dotnet project <dotnet@gentoo.org>
# @AUTHOR:
# Anna Figueiredo Gomes <navi@vlhl.dev>
# Maciej Barć <xgqt@gentoo.org>
# @SUPPORTED_EAPIS: 7 8
# @BLURB: common functions and variables for builds using .NET SDK
# @DESCRIPTION:
# This eclass is designed to provide common definitions for .NET packages.
#
# This eclass does not export any phase functions, for that see
# the "dotnet-pkg" eclass.

case "${EAPI}" in
	7 | 8 )
		:
		;;
	* )
		die "${ECLASS}: EAPI ${EAPI} unsupported."
		;;
esac

if [[ -z ${_DOTNET_PKG_UTILS_ECLASS} ]] ; then
_DOTNET_PKG_UTILS_ECLASS=1

inherit edo

# @ECLASS_VARIABLE: DOTNET_COMPAT
# @REQUIRED
# @PRE_INHERIT
# @DESCRIPTION:
# Allows to choose a slot for dotnet.
#
# Most .NET packages will lock onto one supported .NET major version.
# DOTNET_COMPAT should specify which version was chosen by package upstream.
# In case multiple .NET versions are specified in the project, then the highest
# should be picked by the maintainer.

if [[ ! ${DOTNET_COMPAT} ]] ; then
	die "${ECLASS}: DOTNET_COMPAT not set"
fi

RDEPEND+=" virtual/dotnet-sdk:${DOTNET_COMPAT} "
BDEPEND+=" ${RDEPEND} "

if [[ ${CATEGORY}/${PN} != dev-dotnet/csharp-gentoodotnetinfo ]] ; then
	BDEPEND+=" dev-dotnet/csharp-gentoodotnetinfo "
fi

IUSE+=" debug "

# Needed otherwise the binaries may break.
RESTRICT+=" strip "

# Everything is built by "dotnet".
QA_PREBUILT=".*"

# Special .NET SDK environment variables.
# Setting them either prevents annoying information from being generated
# or stops services that may interfere with a clean package build.
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export DOTNET_NOLOGO=1
export DOTNET_SKIP_FIRST_TIME_EXPERIENCE=1
export MSBUILDDISABLENODEREUSE=1
export POWERSHELL_UPDATECHECK=0
# Overwrite selected MSBuild properties ("-p:XYZ").
export UseSharedCompilation=false

# @ECLASS_VARIABLE: DOTNET_RUNTIME
# @DEFAULT_UNSET
# @DESCRIPTION:
# Sets the runtime used to build a package.
#
# This variable is set automatically by the "dotnet-pkg-utils_setup" function.

# @ECLASS_VARIABLE: DOTNET_EXECUTABLE
# @DEFAULT_UNSET
# @DESCRIPTION:
# Sets path of a "dotnet" executable.
#
# This variable is set automatically by the "dotnet-pkg-utils_setup" function.

# @ECLASS_VARIABLE: DOTNET_CONFIGURATION
# @DEFAULT_UNSET
# @DESCRIPTION:
# Configuration value passed to "dotnet" in the compile phase.
# Is either Debug or Release, depending on the "debug" USE flag.
#
# This variable is set automatically by the "dotnet-pkg-utils_setup" function.

# @ECLASS_VARIABLE: DOTNET_OUTPUT
# @DEFAULT_UNSET
# @DESCRIPTION:
# Path of the output directory, where the package artifacts are placed during
# the building of packages with "dotnet-pkg-utils_build" function.
#
# This variable is set automatically by the "dotnet-pkg-utils_setup" function.

# @ECLASS_VARIABLE: DOTNET_PROJECT
# @DEFAULT_UNSET
# @DESCRIPTION:
# Path to the main .NET project file (.csproj, .fsproj, .vbproj) that is
# used by default by "dotnet-pkg-utils_build" function.
# If this variable is unset the functions using it will default to ${S}.
#
# It may also be used to restore only a selected project, but that behavior
# is not the default and has to be defined by the maintainer, eg by using:
# "dotnet-pkg-utils_restore $(dotnet-pkg-utils_get-project)".
#
# In .NET version 6.0 and lower it was possible to build a project solution
# (.sln) immediately with output to a specified directory (--output DIR),
# but versions >=7.0 deprecated this behavior. This means that
# "dotnet-pkg-utils_build" will fail when pointed to a solution or a directory
# containing a solution file.
# Because of this if a package maintainer runs the package restore, build and
# test inside a directory containing a solution file, they have to set this
# variable.
#
# It is up to the maintainer if this variable is set before inheriting
# "dotnet-pkg.eclass", but it is advised that it is set after ${S} is set,
# it should also it integrate with it.
#
# Example:
# @CODE
# SRC_URI="..."
# S="${S}"/src
#
# LICENSE="MIT"
# SLOT="0"
# KEYWORDS="~amd64"
#
# DOTNET_PROJECT="${S}"/DotnetProject
#
# src_prepare() {
#     ...
# @CODE

# @VARIABLE: DOTNET_LAUNCHERDEST
# @INTERNAL
# @DESCRIPTION:
# Sets the path that .NET launchers are installed into by
# the "dotnet-pkg-utils_dolauncher" function.
#
# The function "dotnet-pkg-utils_launcherinto" is able to manipulate this
# variable.
#
# Defaults to "/usr/bin".
DOTNET_LAUNCHERDEST=/usr/bin

# @VARIABLE: DOTNET_LAUNCHERVARS
# @INTERNAL
# @DESCRIPTION:
# Sets additional variables for .NET launchers created by
# the "dotnet-pkg-utils_dolauncher" function.
#
# The function "dotnet-pkg-utils_append_launchervar" is able to manipulate this
# variable.
#
# Defaults to a empty array.
DOTNET_LAUNCHERVARS=()

# @ECLASS_VARIABLE: NUGET_APIS
# @DESCRIPTION:
# Nuget API URLs to use for precompiled nuget package ".nupkg" downloads.
# Set or append to this variable post-inherit, but before calling
# the "nuget_uris" function, preferably just before "SRC_URI".
#
# Example:
# @CODE
# SRC_URI="https://example.com/example.tar.xz"
# NUGET_APIS+=( "https://api.nuget.org/v3-flatcontainer" )
# SRC_URI+=" $(nuget_uris) "
# @CODE
NUGET_APIS=( "https://api.nuget.org/v3-flatcontainer" )

# @ECLASS_VARIABLE: NUGET_PACKAGES
# @DEFAULT_UNSET
# @PRE_INHERIT
# @DESCRIPTION:
# Path from where nugets will be restored from.
# Defaults to ${T}/nugets for use with "nuget_uris" but may be set to a custom
# location to, for example, restore nugets extracted form a prepared archive.
# Do not set this variable in conjunction with non-empty "NUGETS".

# @ECLASS_VARIABLE: NUGETS
# @DEFAULT_UNSET
# @PRE_INHERIT
# @DESCRIPTION:
# String containing all nuget packages that need to be downloaded.
# Used by "nuget_uris".
#
# Example:
# @CODE
# NUGETS="
#	ImGui.NET-1.87.2
#	Config.Net-4.19.0
# "
#
# inherit dotnet-pkg
#
# ...
#
# SRC_URI="$(nuget_uris)"
# @CODE

# @FUNCTION: nuget_uris
# @USAGE: <nuget...>
# @DESCRIPTION:
# Generates the URIs to put in SRC_URI to help fetch dependencies.
# If no arguments provided, uses NUGETS variable.
nuget_uris() {
	debug-print-function ${FUNCNAME} "${@}"

	local -r regex='^([a-zA-Z0-9_.-]+)-([0-9]+\.[0-9]+\.[0-9]+.*)$'
	local nugets

	if (( ${#} != 0 )) ; then
		nugets="${@}"
	elif [[ ${NUGETS} ]] ; then
		nugets="${NUGETS}"
	else
		eerror "NUGETS variable is not defined and nothing passed as argument"
		die "${FUNCNAME}: Can't generate SRC_URI from empty input"
	fi

	local nuget name version nuget_api url
	for nuget in ${nugets} ; do
		[[ ${nuget} =~ ${regex} ]] ||
			die "${FUNCNAME}: Could not parse given nuget: ${nuget}"

		name="${BASH_REMATCH[1]}"
		version="${BASH_REMATCH[2]}"

		for nuget_api in "${NUGET_APIS[@]}" ; do
			case "${nuget_api}" in
				*/v2 | */v2/ )
					url="${nuget_api}/package/${name}/${version}
							-> ${name}.${version}.nupkg"
					;;
				* )
					url="${nuget_api}/${name}/${version}/${name}.${version}.nupkg"
					;;
			esac
			echo "${url}"
		done
	done
}

# @FUNCTION: dotnet-pkg-utils_get-output
# @USAGE: <name>
# @DESCRIPTION:
# Return a specially constructed name of a directory for output of
# "dotnet build" artifacts ("--output" flag, see "dotnet-pkg-utils_build").
#
# It is very rare that a maintainer would use this function in an ebuild.
#
# This function is used inside "dotnet-pkg-utils_setup".
dotnet-pkg-utils_get-output() {
	debug-print-function ${FUNCNAME} "${@}"

	[[ ! ${DOTNET_CONFIGURATION} ]] &&
		die "${FUNCNAME}: DOTNET_CONFIGURATION is not set."

	echo "${WORKDIR}"/${1}_net${DOTNET_COMPAT}_${DOTNET_CONFIGURATION}
}

# @FUNCTION: dotnet-pkg-utils_setup
# @DESCRIPTION:
# Sets up "DOTNET_EXECUTABLE" variable for later use in "edotnet".
# Also sets up "DOTNET_CONFIGURATION" and "DOTNET_OUTPUT"
# for "dotnet-pkg_src_configure" and "dotnet-pkg_src_compile".
#
# This functions should be called by "pkg_setup".
#
# Used by "dotnet-pkg_pkg_setup" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_setup() {
	local libc="$(usex elibc_musl "-musl" "")"
	if use amd64 ; then
		DOTNET_RUNTIME=linux${libc}-x64
	elif use x86 ; then
		DOTNET_RUNTIME=linux${libc}-x86
	elif use arm ; then
		DOTNET_RUNTIME=linux${libc}-arm
	elif use arm64 ; then
		DOTNET_RUNTIME=linux${libc}-arm64
	else
		die "${FUNCNAME}: Unsupported architecture: ${ARCH}"
	fi

	local _dotnet
	for _dotnet in dotnet{,-bin}-${DOTNET_COMPAT} ; do
		if type ${_dotnet} >/dev/null 2>&1 ; then
			DOTNET_EXECUTABLE=${_dotnet}
			DOTNET_EXECUTABLE_PATH="$(command -v ${_dotnet})"
			break
		fi
	done

	einfo "Using dotnet SDK \"${DOTNET_EXECUTABLE}\" from \"${DOTNET_EXECUTABLE_PATH}\"."

	# Mainly to prevent "NUGET_PACKAGES" environment variable leakage.
	if [[ "${NUGETS}" ]] || [[ ! "${NUGET_PACKAGES}" ]] ; then
		NUGET_PACKAGES="${T}"/nugets
		mkdir -p "${NUGET_PACKAGES}" || die
	fi
	export NUGET_PACKAGES

	# Link "DOTNET_EXECUTABLE" to "dotnet" only for the package build.
	local dotnet_spoof_path="${T}"/dotnet_spoof/${DOTNET_COMPAT}
	mkdir -p "${dotnet_spoof_path}" || die
	ln -s "${DOTNET_EXECUTABLE_PATH}" "${dotnet_spoof_path}"/dotnet || die
	export PATH="${dotnet_spoof_path}:${PATH}"

	# The picked "DOTNET_EXECUTABLE" should set "DOTNET_ROOT" internally
	# and not rely upon this environment variable.
	unset DOTNET_ROOT

	DOTNET_CONFIGURATION=$(usex debug Debug Release)

	DOTNET_OUTPUT="$(dotnet-pkg-utils_get-output ${P})"
}

# @FUNCTION: dotnet-pkg-utils_remove-global-json
# @USAGE: [directory]
# @DESCRIPTION:
# Remove the "global.json" if it exists.
# The file in question might lock target package to a specified .NET
# version, which might be unnecessary (as it is in most cases).
#
# Optional "directory" argument defaults to the current directory path.
#
# Used by "dotnet-pkg_src_prepare" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_remove-global-json() {
	debug-print-function ${FUNCNAME} "${@}"

	local file="${1:-.}"/global.json

	if [[ -f "${file}" ]] ; then
		ebegin "Removing the global.json file"
		rm "${file}"
		eend ${?} || die "${FUNCNAME}: failed to remove ${file}"
	fi
}

# @FUNCTION: edotnet
# @USAGE: <command> [args...]
# @DESCRIPTION:
# Call dotnet, passing the supplied arguments.
edotnet() {
	debug-print-function ${FUNCNAME} "${@}"

	if [[ ! "${DOTNET_EXECUTABLE}" ]] ; then
	   die "${FUNCNAME}: DOTNET_EXECUTABLE not set. Was dotnet-pkg-utils_setup called?"
	fi

	edo "${DOTNET_EXECUTABLE}" "${@}"
}

# @FUNCTION: dotnet-pkg-utils_info
# @DESCRIPTION:
# Show information about current .NET SDK that is being used.
#
# Depends upon the "gentoo-dotnet-info" program installed by
# the "dev-dotnet/csharp-gentoodotnetinfo" package.
#
# Used by "dotnet-pkg_src_configure" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_info() {
	if [[ ${CATEGORY}/${PN} == dev-dotnet/csharp-gentoodotnetinfo ]] ; then
		debug-print-function "${FUNCNAME}: ${P} is a special package, skipping dotnet-pkg-utils_info"
	elif ! command -v gentoo-dotnet-info >/dev/null ; then
		ewarn "${FUNCNAME}: gentoo-dotnet-info not available"
	else
		gentoo-dotnet-info || die "${FUNCNAME}: failed to execute gentoo-dotnet-info"
	fi
}

# @FUNCTION: dotnet-pkg-utils_foreach-solution
# @USAGE: <function> [directory]
# @DESCRIPTION:
# Execute a function for each solution file (.sln) in a specified directory.
# This function may yield no real results because solutions are discovered
# automatically.
#
# Optional "directory" argument defaults to the current directory path.
#
# Used by "dotnet-pkg_src_configure" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_foreach-solution() {
	debug-print-function ${FUNCNAME} "${@}"

	local solution
	for solution in $(find "${2:-.}" -maxdepth 1 -type f -name "*.sln") ; do
		einfo "Running \"${1}\" for a discovered solution: ${solution}"
		"${1}" "${solution}"
	done
}

# @FUNCTION: dotnet-pkg-utils_get-project
# @DESCRIPTION:
# Returns the value of DOTNET_PROJECT.
# If that variable is unset returns ${S}.
#
# Used by "dotnet-pkg_src_configure" and "dotnet-pkg_src_compile"
# from the "dotnet-pkg" eclass.
#
# For more info see the "DOTNET_PROJECT" variable.
dotnet-pkg-utils_get-project() {
	if [[ "${DOTNET_PROJECT}" ]] ; then
		echo "${DOTNET_PROJECT}"
	else
		echo "${S}"
	fi
}

# @FUNCTION: dotnet-pkg-utils_restore
# @USAGE: [directory]
# @DESCRIPTION:
# Restore the package using "dotnet restore" in a specified directory.
#
# Optional "directory" argument defaults to the current directory path.
#
# Used by "dotnet-pkg_src_configure" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_restore() {
	debug-print-function ${FUNCNAME} "${@}"

	local -a restore_args=(
		--runtime ${DOTNET_RUNTIME}
		--source "${NUGET_PACKAGES}"
		-maxCpuCount:1
	)

	edotnet restore "${restore_args[@]}" "${1:-.}"
}

# @FUNCTION: dotnet-pkg-utils_build
# @USAGE: [directory]
# @DESCRIPTION:
# Build the package using "dotnet build" in a specified directory.
#
# Optional "directory" argument defaults to the current directory path.
#
# Used by "dotnet-pkg_src_compile" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_build() {
	debug-print-function ${FUNCNAME} "${@}"

	local -a build_args=(
		--configuration "${DOTNET_CONFIGURATION}"
		--no-restore
		--no-self-contained
		--nologo
		--output "${DOTNET_OUTPUT}"
		--runtime ${DOTNET_RUNTIME}
		-p:WarningLevel=0
		-maxCpuCount:1
	)

	if ! use debug ; then
		build_args+=(
			-p:StripSymbols=true
			-p:NativeDebugSymbols=false
		)
	fi

	edotnet build "${build_args[@]}" "${1:-.}"
}

# @FUNCTION: dotnet-pkg-utils_test
# @USAGE: [directory]
# @DESCRIPTION:
# Test the package using "dotnet test" in a specified directory.
#
# Optional "directory" argument defaults to the current directory path.
#
# Used by "dotnet-pkg_src_test" from the "dotnet-pkg" eclass.
dotnet-pkg-utils_test() {
	debug-print-function ${FUNCNAME} "${@}"

	local -a test_args=(
		--configuration "${DOTNET_CONFIGURATION}"
		--no-restore
		--nologo
		-maxCpuCount:1
	)

	edotnet test "${test_args[@]}" "${1:-.}"
}

# @FUNCTION: dotnet-pkg-utils_install
# @USAGE: [directory]
# @DESCRIPTION:
# Install the contents of "DOTNET_OUTPUT" into a directory, defaults to
# "/usr/share/${PN}".
#
# Installation directory is relative to "ED".
dotnet-pkg-utils_install() {
	debug-print-function ${FUNCNAME} "${@}"

	local installation_directory="${1:-/usr/share/${P}}"

	dodir "${installation_directory}"
	cp -r "${DOTNET_OUTPUT}"/* "${ED}"/"${installation_directory}"/ || die
}

# @FUNCTION: dotnet-pkg-utils_launcherinto
# @USAGE: <directory>
# @DESCRIPTION:
# Changes the path .NET launchers are installed into via subsequent
# "dotnet-pkg-utils_dolauncher" calls.
#
# For more info see the "DOTNET_LAUNCHERDEST" variable.
dotnet-pkg-utils_launcherinto() {
	debug-print-function ${FUNCNAME} "${@}"

	[[ ! "${1}" ]] && die "${FUNCNAME}: no directory specified"

	DOTNET_LAUNCHERDEST="${1}"
}

# @FUNCTION: dotnet-pkg-utils_append_launchervar
# @USAGE: <variable-setting>
# @DESCRIPTION:
# Appends a given variable setting to the "DOTNET_LAUNCHERVARS".
#
# WARNING: This functions modifies a global variable permanently!
# This means that all launchers created in subsequent
# "dotnet-pkg-utils_dolauncher" calls of a given package will have
# the given variable set.
#
# Example:
# @CODE
# dotnet-pkg-utils_append_launchervar "DOTNET_EnableAlternateStackCheck=1"
# @CODE
#
# For more info see the "DOTNET_LAUNCHERVARS" variable.
dotnet-pkg-utils_append_launchervar() {
	debug-print-function ${FUNCNAME} "${@}"

	[[ ! "${1}" ]] && die "${FUNCNAME}: no variable setting specified"

	DOTNET_LAUNCHERVARS+=( "${1}" )
}

# @FUNCTION: dotnet-pkg-utils_dolauncher
# @USAGE: <executable-path> [filename]
# @DESCRIPTION:
# Make a wrapper script to launch an executable built from a .NET package.
#
# If no file name is given, the `basename` of the executable is used.
#
# Parameters:
# ${1} - path of the executable to launch,
# ${2} - filename of launcher to create (optional).
#
# Example:
# @CODE
# dotnet-pkg-utils_install
# dotnet-pkg-utils_dolauncher /usr/share/${P}/${PN^}
# @CODE
#
# The path is prepended by "EPREFIX".
dotnet-pkg-utils_dolauncher() {
	debug-print-function ${FUNCNAME} "${@}"

	local executable_path executable_name

	if [[ "${1}" ]] ; then
		local executable_path="${1}"
		shift
	else
		die "${FUNCNAME}: No executable path given."
	fi

	if [[ ${#} = 0 ]] ; then
		executable_name="$(basename "${executable_path}")"
	else
		executable_name="${1}"
		shift
	fi

	local executable_target="${T}/${executable_name}"

	cat > "${executable_target}" <<-EOF
	#!/bin/sh

	# Lanucher script for ${executable_path} (${executable_name}),
	# created from package "${CATEGORY}/${P}",
	# compatible with dotnet version ${DOTNET_COMPAT}.

	for __dotnet_root in ${EPREFIX}/opt/dotnet-sdk{,-bin}-${DOTNET_COMPAT} ; do
		[ -d \${__dotnet_root} ] && break
	done

	DOTNET_ROOT="\${__dotnet_root}"
	export DOTNET_ROOT

	$(for var in "${DOTNET_LAUNCHERVARS[@]}" ; do
		echo "${var}"
		echo "export ${var%%=*}"
	done)

	exec "${EPREFIX}${executable_path}" "\${@}"
	EOF

	dodir "${DOTNET_LAUNCHERDEST}"
	exeinto "${DOTNET_LAUNCHERDEST}"
	doexe "${executable_target}"
}

fi
