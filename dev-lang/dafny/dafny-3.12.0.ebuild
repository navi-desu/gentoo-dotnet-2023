# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=6.0
NUGET_PACKAGES="${WORKDIR}"/nuget_packages

inherit check-reqs dotnet-pkg multiprocessing

DESCRIPTION="Dafny is a verification-aware programming language"
HOMEPAGE="https://dafny.org/
	https://github.com/dafny-lang/dafny/"
SRC_URI="
	https://github.com/dafny-lang/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz
	https://dev.gentoo.org/~xgqt/distfiles/deps/${PN}-3.12.0-r1-prebuilt.tar.xz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="
	!dev-lang/dafny-bin
	>=virtual/jre-1.8:*
	sci-mathematics/z3
"
DEPEND=">=virtual/jdk-1.8:*"
BDEPEND="
	${RDEPEND}
	dev-dotnet/coco
	test? (
		dev-python/lit
		dev-python/OutputCheck
	)
"

PATCHES=(
	"${FILESDIR}"/${P}-DafnyCore-csproj.patch
	"${FILESDIR}"/${P}-DafnyRuntime-csproj.patch
)

# The requirements come mostly from big size of prebuilt nugets, see "SRC_URI".
CHECKREQS_DISK_BUILD="3G"

DOTNET_PROJECT="${S}/Source/Dafny/Dafny.csproj"

DOCS=(
	CODE_OF_CONDUCT.md
	CONTRIBUTING.md
	NOTICES.txt
	README.md
	RELEASE_NOTES.md
	docs/{DafnyCheatsheet,DafnyCheatsheet}.pdf
)

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_prepare() {
	# Remove bad tests.
	local bad_tests="$(cat "${FILESDIR}"/${P}-failing-tests.txt)"
	local bad_test
	for bad_test in ${bad_tests} ; do
		rm "${S}"/Test/${bad_test} || die
	done

	# Update lit's "lit.site.cfg" file.
	local dotnet_exec="${DOTNET_EXECUTABLE} exec ${DOTNET_OUTPUT}"
	local lit_config="${S}"/Test/lit.site.cfg
	sed "/^defaultDafnyExecutable/s|=.*|= '${dotnet_exec}/Dafny.dll '|" \
		-i "${lit_config}" || die "failed to update ${lit_config}"
	sed "/^dafnyExecutable/s|=.*|= '${dotnet_exec}/Dafny.dll '|" \
		-i "${lit_config}" || die "failed to update ${lit_config}"
	sed "/^testDafnyExecutable/s|=.*|= '${dotnet_exec}/TestDafny.dll for-each-compiler '|" \
		-i "${lit_config}" || die "failed to update ${lit_config}"
	sed "/^defaultServerExecutable/s|=.*|= '${dotnet_exec}/DafnyServer.dll'|" \
		-i "${lit_config}" || die "failed to update ${lit_config}"
	sed "/^serverExecutable/s|=.*|= '${dotnet_exec}/DafnyServer.dll'|" \
		-i "${lit_config}" || die "failed to update ${lit_config}"

	dotnet-pkg_src_prepare
}

src_configure() {
	dotnet-pkg_src_configure
	dotnet-pkg-utils_restore "${S}/Source/TestDafny/TestDafny.csproj"
}

src_compile () {
	# Build dependency-less DafnyRuntime JAR.
	local dafny_runtime_java="${S}"/Source/DafnyRuntime/DafnyRuntimeJava/
	mkdir -p "${dafny_runtime_java}"/build/libs/ || die
	pushd "${dafny_runtime_java}"/build || die
	javac -d ./ "${dafny_runtime_java}"/src/main/java/dafny/*.java || die
	jar cvf DafnyRuntime.jar ./dafny/* || die
	cp DafnyRuntime.jar "${dafny_runtime_java}"/build/libs/ || die
	popd || die

	# Build main dotnet package.
	dotnet-pkg_src_compile
	dotnet-pkg-utils_build "${S}/Source/TestDafny/TestDafny.csproj"
}

src_test() {
	lit --threads $(makeopts_jobs) --verbose "${S}"/Test || die "tests failed"
}

src_install() {
	dotnet-pkg-utils_install

	local dafny_exe
	local -a dafny_exes=(
		Dafny
		DafnyDriver
		DafnyLanguageServer
		DafnyServer
		TestDafny
	)
	for dafny_exe in ${dafny_exes[@]} ; do
		dotnet-pkg-utils_dolauncher /usr/share/${P}/${dafny_exe} ${dafny_exe}
	done

	dosym -r /usr/bin/Dafny /usr/bin/dafny
	dosym -r /usr/bin/DafnyServer /usr/bin/dafny-server

	einstalldocs
}
