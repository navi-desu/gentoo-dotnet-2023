# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=6.0
NUGET_PACKAGES="${WORKDIR}"/nuget_packages

inherit check-reqs dotnet-pkg multiprocessing

DESCRIPTION="SMT-based program verifier"
HOMEPAGE="https://github.com/boogie-org/boogie/"
SRC_URI="
	https://github.com/boogie-org/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz
	https://dev.gentoo.org/~xgqt/distfiles/deps/${PN}-2.16.1-prebuilt.tar.xz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="sci-mathematics/z3"
BDEPEND="
	${RDEPEND}
	test? (
		dev-python/lit
		dev-python/OutputCheck
	)
"

# The requirements come mostly from big size of prebuilt nugets, see "SRC_URI".
CHECKREQS_DISK_BUILD="2G"

DOTNET_PROJECT="${S}/Source/BoogieDriver/BoogieDriver.csproj"

DOCS=( README.md )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_prepare() {
	# Remove bad tests.
	local -a bad_tests=(
		civl/inductive-sequentialization/BroadcastConsensus.bpl
		civl/inductive-sequentialization/PingPong.bpl
		livevars/bla1.bpl
		prover/cvc5-offline.bpl
		prover/cvc5.bpl
		prover/z3mutl.bpl
		test0/MaxKeepGoingSplits.bpl
		test15/CaptureInlineUnroll.bpl
		test15/CaptureState.bpl
		test15/CommonVariablesPruning.bpl
	)
	local bad_test
	for bad_test in ${bad_tests[@]} ; do
		rm "${S}"/Test/${bad_test} || die
	done

	# Update the boogieBinary variable.
	sed "/^boogieBinary/s|= .*|= '${DOTNET_OUTPUT}/BoogieDriver.dll'|" \
		-i "${S}"/Test/lit.site.cfg || die "failed to update lit.site.cfg"

	dotnet-pkg_src_prepare
}

src_test() {
	lit --threads $(makeopts_jobs) --verbose "${S}"/Test || die "tests failed"
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/BoogieDriver boogie

	einstalldocs
}
