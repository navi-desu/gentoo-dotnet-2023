# Gentoo-Dotnet-2023

Experimental Gentoo Linux for packages related to the dotnet SDK.

## About

This is a experimental Gentoo Linux overlay that aims to bring greater
dotnet SDK support to Gentoo.

This started off with a `::gentoo` repository pull request #29309:
https://github.com/gentoo/gentoo/pull/29309/

## Development

### Finding required NUGETS

1. Cd into a directory that would be used for `${S}`.
2. Set proper `DOTNET_COMPAT` variable,
   for example: `DOTNET_COMPAT=7.0`.
3. Run the commands below, a list of required nugets will be located inside
   the `nugets.txt` file.

``` shell
nugets_txt="$(pwd)"/nugets.txt
export NUGET_PACKAGES="$(pwd)"/nuget_packages

for runtime in linux linux-{,musl-}{arm,arm64,x64,x86} ; do
    echo "Restore for runtime: ${runtime}"
    dotnet-bin-${DOTNET_COMPAT} restore             \
        --runtime ${runtime}                        \
        -maxCpuCount:1                              \
        .
done

pushd "${NUGET_PACKAGES}"
for nuget in */* ; do
    echo "${nuget}" | sed "s#/#-#" >> "${nugets_txt}"
done
popd
```

## Related bugs

- https://bugs.gentoo.org/833356
- https://bugs.gentoo.org/882569
- https://bugs.gentoo.org/900597
- https://github.com/gentoo/gentoo/pull/29309
